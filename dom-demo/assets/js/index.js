// INSPECTING DOM -------------------------------------------
// console.log(document.title)
// console.log(document.URL)
// console.log(document.head)


// TARGETING DOM ELEMENT -------------------------------------

// const input_todo = document.getElementById("input_todo")
// const todos = document.getElementsByClassName("todo")
// const div = document.getElementsByTagName("div")


// console.log(div)

// for (i=0;i<todos.length;i++){
//     console.log(todos[i])
// }

// TARGETING DOM ELEMENT (QUERY SELECTOR) -------------------------------------

// const todos = document.querySelector(".todo")
// const todos = document.querySelectorAll(".todo")

// console.log(todos)


// CREATE DOM ELEMENT -------------------------------------------------

// let newDiv = document.createElement("div")
// let header = document.getElementsByTagName("header")[0]

// newDiv.innerHTML = "<h1>Test</h1>"
// newDiv.style.backgroundColor = "blue"
// newDiv.id = "2"
// newDiv.className = "todo"

// console.log(header)

// header.appendChild(newDiv)

// EVENT LISTENER  -------------------------------------------------


// const input = document.getElementById("input_button")
const input_todo = document.getElementById("input_todo")
// input.addEventListener("click", handleClick)
// input.addEventListener("dblclick", handleClick)

input_todo.addEventListener("keypress", handleKeypress)

// function handleClick() {
//     console.log("Clicked!")
// }

const buffer = ""

function handleKeypress(e) {
    console.log(e.key)
    console.log(e.target.id)
    console.log(e.target.className)
    console.log(e.target.className)
}

// console.log(input_todo)