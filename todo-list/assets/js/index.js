let todo_list = [{
        todo: "Learn React",
        done: false
    },
    {
        todo: "Learn React Native",
        done: false
    }, {
        todo: "Learn NodeJs",
        done: false
    }, {
        todo: "Learn ES6",
        done: false
    }
]

function create_todo(todo) {
    todo_list.push({
        todo: todo,
        done: false
    })
}

function read_todo() {
    console.log(todo_list)
}

function update_todo(index, key, value) {
    todo_list[index][key] = value
}

function delete_todo(index) {
    todo_list.splice(index, 1);
}

function search_todo(key, value) {
    let const_result = todo_list.filter(todo => todo[key] === value)
    console.log(const_result)
}